
# LeGOO / EffectorK source code

The source code is now available @ [https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/KGB](https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/KGB/)



# Publications

- <strong>LeGOO: An Expertized Knowledge Database For The Model Legume Medicago truncatula.</strong>
Sébastien Carrère, Marion Verdenaud, Clare Gough, Jérôme Gouzy, Pascal Gamas.
<i>Plant and Cell Physiology. 2019; pcs177. doi: 10.1093/pcp/pcz177.</i>

- <strong> EffectorK, a comprehensive resource to mine for Ralstonia, Xanthomonas, and other published effector interactors in the Arabidopsis proteome. </strong>
Manuel Gonzalez-Fuente ; Sebastien Carrere ; Dario Monachello ; Benjamin G. Marsella ; Anne-Claire Cazale ; Claudine Zischek ; Raka M. Mitra ; Nathalie Reze ; Ludovic Cottret ; M. Shahid Mukhtar ; Claire Lurin Laurent D. Noel ; Nemo Peeters . + 
<i>Molecular Plant Pathology. 2020; doi: 10.1111/mpp.12965</i>


# Contact
Sebastien.Carrere@inrae.fr
